; group1_team14_8369_8517_lab3.asm
; Deligiannidis Pantelis 8369, Misailidis Panagiotis 8517
; Group 1, Omada 14
;

.include "m16def.inc"
.def temp=r16
.def input=r17      ; The mode user enters is stored in the least significant 4 bits
                    ;   all bits but 1 will be used as flags
                    ;   bit 7 clear -> 18 seconds washing time mode
                    ;   bit 6 clear -> 12 seconds washing time mode
                    ;   bit 5 clear -> 8 seconds washing time mode
                    ;   bit 4 clear -> 4 seconds washing time mode
                    ;   bit 3 clear -> Wring enabled
                    ;   bit 2 clear -> Rinse enabled
                    ;   bit 0 clear -> Prewash enabled

.cseg               ; Code segment
.org 0x00           ; Define starting address of program

; Initializations

ldi temp, HIGH(RAMEND)      ; Initialize stack pointer
out SPH, temp
ldi temp, LOW(RAMEND)
out SPL, temp

ser temp            ; Set all bits of Reg Temp to 1
out DDRB, temp
out PORTB, temp     ; Turn off all LEDs

clr temp
out DDRD, temp      ; Setting PORTD as input port

; Main Program
main:

ser input

rcall waitForSW6

rcall readMode

rcall setFlags

rcall waitForSW6

sbrs input, 0
    rcall preWash

rcall mainWash

sbrs input, 2
    rcall rinse

sbrs input, 3
    rcall wring

rcall LED7for5Sec

; Restart (not required)
rjmp main


;------------*****------------
; Here we wait for SW6 to be pushed in order to proceed

waitForSW6:
    cbi PORTB, 6        ; Blinking LED 6 while we wait (not required)
    rcall delay200ms

    sbi PORTB, 6
    rcall delay200ms

    sbic PIND, 6
    rjmp waitForSW6
ret

;------------*****------------
; Here we give the user 10 seconds to push SW2 SW3 SW4 SW5
; Delay of 10sec
; Assuming 4MHz

readMode:
    ldi r27,60      ; Should be 60 for 10 second delay TO FIX*********

    outer_outer_loopRM:
        ldi r26,255

        outer_loopRM:
            ldi r25,255

            inner_loopRM:
                sbis PIND, 2
                    andi input, 0b11111110
                sbis PIND, 3
                    andi input, 0b11111101
                sbis PIND, 4
                    andi input, 0b11111011
                sbis PIND, 5
                    andi input, 0b11110111
            dec r25
            brne inner_loopRM
        dec r26
        brne outer_loopRM
    dec r27
    brne outer_outer_loopRM
ret

;------------*****------------
; Determine washing mode (4/8/12/18 sec)
;   and set the 7-4 flags of input

setFlags:
    mov temp, input
    lsr temp                    ; Dropping the SW2 bit
    andi temp, 0b00000011       ; Isolating the SW3 SW4 bits (mode bits)

    brne not0
        andi input, 0b11101111   ; Setting the 4 sec flag
    not0:

    cpi temp, 1
    brne not1
        andi input, 0b11011111   ; Setting the 8 sec flag
    not1:

    cpi temp, 2
    brne not2
        andi input, 0b10111111   ; Setting the 12 sec flag
    not2:

    cpi temp, 3
    brne not3
        andi input, 0b01111111   ; Setting the 18 sec flag
    not3:

ret

;------------*****------------
; Pre wash should last 4 seconds
; Delay of 1sec
; Assuming 4MHz

preWash:
    push r27
    push r26
    push r25
    
    cbi PORTB, 2

    ldi r27, 4      ; Should be 6 for 1 second delay

    outer_outer_loopPW:
        ldi r26, 255

        outer_loopPW:
            ldi r25, 255

            inner_loopPW:
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
            dec r25
            brne inner_loopPW
        dec r26
        brne outer_loopPW
    dec r27
    brne outer_outer_loopPW

    sbi PORTB, 2

    pop r25
    pop r26
    pop r27
ret

;------------*****------------
; Main wash loop        ; FIX TIMES ************
; Delay of Xsec depending on mode
; Assuming 4MHz
; Using r25, r26, r27

mainWash:
    push r27
    push r26
    push r25

    cbi PORTB, 3

    sbrs input, 7           ; Choosing mode
        ldi r27, 18        ; Should be 60 for 10 second delay
    sbrs input, 6
        ldi r27, 12         ; Should be 60 for 10 second delay
    sbrs input, 5
        ldi r27, 8         ; Should be 60 for 10 second delay
    sbrs input, 4
        ldi r27, 4         ; Should be 60 for 10 second delay

    outer_outer_loopMW:
        ldi r26, 255

        outer_loopMW:
            ldi r25, 255

            inner_loopMW:
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
            dec r25
            brne inner_loopMW
        dec r26
        brne outer_loopMW
    dec r27
    brne outer_outer_loopMW

    sbi PORTB, 3

    pop r25
    pop r26
    pop r27
ret

;------------*****------------
; Rinse should last 1 seconds
; Delay of 1sec
; Assuming 4MHz

rinse:
    push r27
    push r26
    push r25

    cbi PORTB, 4

    ldi r27, 1      ; Should be 6 for 1 second delay

    outer_outer_loopR:
        ldi r26, 255

        outer_loopR:
            ldi r25, 255

            inner_loopR:
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
            dec r25
            brne inner_loopR
        dec r26
        brne outer_loopR
    dec r27
    brne outer_outer_loopR

    sbi PORTB, 4
    
    pop r25
    pop r26
    pop r27
ret

;------------*****------------
; Wring should last 2 seconds
; Delay of 2sec
; Assuming 4MHz

wring:
    push r27
    push r26
    push r25

    cbi PORTB, 5

    ldi r27, 2      ; Should be 6 for 1 second delay

    outer_outer_loopW:
        ldi r26, 255

        outer_loopW:
            ldi r25, 255

            inner_loopW:
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
                rcall checkInterrupts
            dec r25
            brne inner_loopW
        dec r26
        brne outer_loopW
    dec r27
    brne outer_outer_loopW

    sbi PORTB, 5

    pop r25
    pop r26
    pop r27
ret

;------------*****------------
; Turn on LED7 for 5 sec

LED7for5Sec:
    cbi PORTB, 7

    rcall delay1sec
    rcall delay1sec
    rcall delay1sec
    rcall delay1sec
    rcall delay1sec

    sbi PORTB, 7
ret

;------------*****------------
; Here we check all interrupts
; Door opens    - SW0
; Overload      - SW1
; Water Supply  - SW7

checkInterrupts:
    sbis PIND, 0
        rcall doorOpened
    sbis PIND, 1
        rcall overloaded
    sbis PIND, 7
        rcall waterSupplyCut
ret

doorOpened:             ; Blink LED0 every 1 sec, waiting for SW0 to be pressed again
    cbi PORTB, 0
    rcall delay1sec

    sbis PIND, 0
        rjmp retFromInterrupt

    sbi PORTB, 0
    rcall delay1sec

    sbis PIND, 0
        rjmp retFromInterrupt

rjmp doorOpened

overloaded:             ; Blink LED1 every 1 sec, waiting for SW1 to be pressed again
    cbi PORTB, 1
    rcall delay1sec

    sbis PIND, 1
        rjmp retFromInterrupt

    sbi PORTB, 1
    rcall delay1sec

    sbis PIND, 1
        rjmp retFromInterrupt

rjmp overloaded

waterSupplyCut:         ; Blink LED1 every 1 sec, LED6 always on, waiting for SW7 to be pressed again
    cbi PORTB, 6

    cbi PORTB, 1
    rcall delay1sec

    sbis PIND, 7
        rjmp retFromInterrupt

    sbi PORTB, 1
    rcall delay1sec

    sbis PIND, 7
        rjmp retFromInterrupt

rjmp waterSupplyCut

retFromInterrupt:
    rcall delay20ms         ; Debounce

    waitForSW0Release:
        sbis PIND, 0
    rjmp waitForSW0Release

    waitForSW1Release:
        sbis PIND, 1
    rjmp waitForSW1Release

    waitForSW7Release:
        sbis PIND, 7
    rjmp waitForSW7Release

    sbi PORTB, 0            ; Turn off LED0 in case it was left on
    sbi PORTB, 1            ; Turn off LED0 in case it was left on
    sbi PORTB, 6            ; Turn off LED0 in case it was left on

    rcall delay20ms         ; Debounce
ret

;------------*****------------
; Delay of 20ms
; Assuming 4MHz

delay20ms:
    push r25
    push r26

    ldi r26, 31

    outer_loop20:

        ldi r25, 255

        inner_loop20:

            nop
            nop
            nop
            nop
            nop
            nop
            nop
        dec r25
        brne inner_loop20
    dec r26
    brne outer_loop20

    pop r26
    pop r25
ret

;------------*****------------
; Delay of 200ms
; By calling delay20ms 10 times

delay200ms:
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms
    rcall delay20ms

ret

;------------*****------------
; Delay of 1sec
; Assuming 4MHz

delay1sec:
    push r27
    push r26
    push r25

    ldi r27, 4      ; Should be 6 for 1 second delay

    outer_outer_loop1:
        ldi r26,255

        outer_loop1:
            ldi r25,255

            inner_loop1:
                nop
                nop
                nop
                nop
                nop
            dec r25
            brne inner_loop1
        dec r26
        brne outer_loop1
    dec r27
    brne outer_outer_loop1

    pop r25
    pop r26
    pop r27
ret

