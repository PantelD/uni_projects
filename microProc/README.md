## USEFUL NOTES

**INSTRUCTIONS APPLIED ONLY TO R16-R31**

* SUBCI
* SUBI
* CPI
* ANDI
* ORI
* LDI Rd, K       ;load immediate Rd<-K

**INSTRUCTIONS APPLIED ONLY TO $00-$1F I/O REGISTERS**

* SBI PORTD, 7    ;set any bit to 1
* CBI PORTD, 7    ;set any bit to 0

**SPECIAL REGISTERS**

* R0    
  * LPM             ;load program memory R0<-(Z)

* R26-R31
  * XYZ

