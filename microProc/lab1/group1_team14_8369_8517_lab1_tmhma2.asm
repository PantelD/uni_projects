; group1_team14_8369_8517_lab1_tmhma2.asm
; Deligiannidis Pantelis 8369, Misailidis Panagiotis 8517
; Group 1, Omada 14
;

.include "m16def.inc"
.def input=r25  ; Stores input from PIND
.def temp=r24

.cseg           ; code segment
.org 0x00       ; define starting address of program

; Storing AEMs in memory

AEM1: .db 8,3,6,9   ; store AEMs in Program Memory
AEM2: .db 8,5,1,7

; Initializing Stack Pointer

ldi temp, HIGH(RAMEND)            ; HIGH-Byte der obersten RAM-Adresse
out SPH, temp
ldi temp, LOW(RAMEND)             ; LOW-Byte der obersten RAM-Adresse
out SPL, temp

ser temp    	; Set all bits of Reg Temp to 1
out DDRB,temp
out PORTB,temp	; Turn off all LEDs

clr temp		
out DDRD,temp	; Setting PORTD as input port

clr r30         ; initialize Z pointer
clr r31

lpm r16,Z+ 	    ; load AEMs from Program Memory
lpm r17,Z+
lpm r18,Z+
lpm r19,Z+
lpm r20,Z+
lpm r21,Z+
lpm r22,Z+
lpm r23,Z+

cp r20,r16			; compare first digit
brlo AEM1notSmaller
cp r16,r20
brlo AEM1Smaller

cp r21,R17			; second
brlo AEM1notSmaller
cp r17,r21
brlo AEM1Smaller

cp r22,r18			; third
brlo AEM1notSmaller
cp r18,r22
brlo AEM1Smaller

cp r19,r23			; fourth
brge AEM1notSmaller

AEM1Smaller:

    ; AEM1 < AEM2
    ser r29				; Setting up our comparison flag
    rjmp main_loop

AEM1notSmaller:

    ; AEM1 >= AEM2
    clr r29

main_loop:

	in input,PIND
	
	sbrs input,0
	rcall ButtonPressed
	sbrs input,1
	rcall ButtonPressed
	sbrs input,2
	rcall ButtonPressed
	sbrs input,3
	rcall ButtonPressed
	sbrs input,7
	rcall ButtonPressed

	rjmp main_loop

ButtonPressed:

	rcall delay20ms		; Waiting for stabilization

	ser temp					; Turn off all LEDs
	out PORTB,temp 		; In case SW7 was pressed (means LEDs were left on)

	WaitingForReleaseSW0:		; All buttons must be realised
		sbis PIND,0
	rjmp WaitingForReleaseSW0

	WaitingForReleaseSW1:
		sbis PIND,1
	rjmp WaitingForReleaseSW1

	WaitingForReleaseSW2:
		sbis PIND,2
	rjmp WaitingForReleaseSW2

	WaitingForReleaseSW3:
		sbis PIND,3
	rjmp WaitingForReleaseSW3

	WaitingForReleaseSW7:
		sbis PIND,7
	rjmp WaitingForReleaseSW7	

	sbrs input,7		; If SW7 was initially pressed
	rcall SW7
	sbrs input,7
	rjmp EndSW7

	sbrs r29,0
	rjmp AEM1notSmallerSW

	;AEM1Smaller:		; AEM1 < AEM2

		sbrs input,0	; If SW0 was initially pressed
		mov temp,r22
		sbrs input,1	; If SW1 was initially pressed
		mov temp,r20
		sbrs input,2	; If SW2 was initially pressed
		mov temp,r18
		sbrs input,3	; If SW3 was initially pressed
		mov temp,r16

		swap temp

		sbrs input,0
		OR temp,r23
		sbrs input,1
		OR temp,r21
		sbrs input,2
		OR temp,r19
		sbrs input,3
		OR temp,r17

		rjmp EndSW0123

	AEM1notSmallerSW:	; AEM1 >= AEM2

		sbrs input,0	; If SW0 was initially pressed
		mov temp,r18
		sbrs input,1	; If SW1 was initially pressed
		mov temp,r16
		sbrs input,2	; If SW2 was initially pressed
		mov temp,r22
		sbrs input,3	; If SW3 was initially pressed
		mov temp,r20

		swap temp

		sbrs input,0
		OR temp,r19
		sbrs input,1
		OR temp,r17
		sbrs input,2
		OR temp,r23
		sbrs input,3
		OR temp,r21

	EndSW0123:

	com temp		; Compliment because of STK500
	out PORTB,temp

	rcall delay10sec

	ser temp		
	out PORTB,temp 	; Turn off all LEDs

	EndSW7:
	
	ret

SW7:
	
	mov temp,r23
	lsr temp     	; LSB of AEM2 is saved in C
	mov temp,r19
	sbrs r29,0		; If AEM1 >= AEM2 then
	lsr temp    	; LSB of AEM1 is saved in C 
	brcs BigAEMisOdd

	BigAEMisEven:
		cbi PORTB,1

	BigAEMisOdd:

	mov temp,r19
	lsr temp     	; LSB of AEM1 is saved in C
	mov temp,r23
	sbrs r29,0  	; If AEM1 >= AEM2 then
	lsr temp     	; LSB of AEM1 is saved in C
	brcc SmallAEMisEven

	SmallAEMisOdd:
		cbi PORTB,0

	SmallAEMisEven:

	ret

;------------*****------------
; Delay of 20ms
; Assuming 4MHz
; Using r25, r26

delay20ms: 		
	
	push r25

	ldi r26,31

	outer_loop:

	ldi r25,255

	inner_loop:

	nop
	nop
	nop
	nop
	nop
	nop
	nop
	dec r25
	brne inner_loop
	dec r26
	brne outer_loop
	
	pop r25
	ret

;------------*****------------
; Delay of 10sec
; Assuming 4MHz
; Using r25, r26, r27

delay10sec:

	push r25

	ldi r27,80		; Should be 60 for 10 second delay		

	outer_outer_loop:
	ldi r26,255

	outer_loop10:
	ldi r25,255

	inner_loop10:
	nop
	nop
	nop
	nop
	nop
	dec r25
	brne inner_loop10
	dec r26
	brne outer_loop10
	dec r27
	brne outer_outer_loop

	pop r25
	ret

