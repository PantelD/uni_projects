; group1_team14_8369_8517_lab1_tmhma1.asm
; Deligiannidis Pantelis 8369, Misailidis Panagiotis 8517
; Group 1, Omada 14
;
; Part I:   Save AEM digits in Program Memory
; Part II;  If AEM1 < AEM2 then print 2 last digits of AEM1 on LEDs 7-0
; Part III: Turn on last 2 LEDs based on the array given

.include "m16def.inc"
.def temp=r24

.cseg       ; Code segment
.org 0x00   ; Define starting address of program

; Part I

AEM1: .db 0x38,0x33,0x36,0x39   ; Store AEMs in Program Memory
AEM2: .db 0x38,0x35,0x31,0x37   ; as ASCII

; Part II

ser temp            ; Set all bits of Reg Temp to 1
out DDRB, temp
out PORTB, temp     ; Turn off all LEDs

clr temp            ; Setting PIND as input
out DDRB, temp      ; We are later using SW7

clr r30             ; initialize Z pointer
clr r31

lpm r16, Z+         ; load AEMs from Program Memory
lpm r17, Z+
lpm r18, Z+
lpm r19, Z+
lpm r20, Z+
lpm r21, Z+
lpm r22, Z+
lpm r23, Z+

subi r16, 0x30      ; Convert digits from ASCII to binary format
subi r17, 0x30
subi r18, 0x30
subi r19, 0x30
subi r20, 0x30
subi r21, 0x30
subi r22, 0x30
subi r23, 0x30

cp r20,r16          ; Compare first digit
brlo AEM1notSmaller
cp r16,r20
brlo AEM1Smaller

cp r21,R17          ; second
brlo AEM1notSmaller
cp r17,r21
brlo AEM1Smaller

cp r22,r18          ; third
brlo AEM1notSmaller
cp r18,r22
brlo AEM1Smaller

cp r19,r23          ; fourth
brge AEM1notSmaller

AEM1Smaller:
    mov temp,r18    ; Setting LEDs 7-4
    swap temp
    or temp,r19     ; Setting LEDs 3-0

    com temp        ; Inverting leds because STK500
    out PORTB,temp

    ser r27         ; r27 is used as a comparison flag
    rjmp ComparisonOver

AEM1notSmaller:
    clr r27

ComparisonOver:

; Part III

waitingForSW0:      ; Waiting for SW7 to be pressed to move
    sbic PIND, 7    ; on to the next part of the exercise
rjmp waitingForSW0  ; Without this part there would be no time to see the LEDs from PART II

ser temp    ; Turning off all LEDs
out PORTB,temp

mov temp,r23
lsr temp    ; LSB of AEM2 is saved in C
sbrs r27,0  ; If AEM1 >= AEM2 then
lsr r19     ; LSB of AEM1 is saved in C 
brcs BigAEMisOdd

BigAEMisEven:
    cbi PORTB,1

BigAEMisOdd:

lsr r19     ; LSB of AEM1 is saved in C
sbrs r27,0  ; If AEM1 >= AEM2 then
lsr r23     ; LSB of AEM1 is saved in C
brcc SmallAEMisEven

SmallAEMisOdd:
    cbi PORTB,0

SmallAEMisEven:
ret
