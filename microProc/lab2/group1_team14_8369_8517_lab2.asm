; group1_team14_8369_8517_lab2.asm
; Deligiannidis Pantelis 8369, Misailidis Panagiotis 8517
; Group 1, Omada 14
;

.include "m16def.inc"
.def temp=r16
.def myFlags=r17
.def input=r18		; The passwords user enters
.def password=r19	; The correct password
.def LED0State=r20	; Used to blink LED0 after 1 failure

.cseg           	; Code segment
.org 0x00       	; Define starting address of program

ldi temp, HIGH(RAMEND)      ; Initialize stack pointer
out SPH, temp
ldi temp, LOW(RAMEND)
out SPL, temp

ser temp            ; Set all bits of Reg Temp to 1
out DDRB, temp
out PORTB, temp     ; Turn off all LEDs

clr temp        
out DDRD, temp      ; Setting PORTD as input port

; Register myFlags is used for various flags
; 	bit 6 - If 1, there has been 1 failed attempt
clr myFlags
ser password
ser input
clr LED0State

rcall setUpPass


clr temp			; Blink all LEDs twice after password is set
out PORTB, temp
rcall delay200ms
ser temp
out PORTB, temp
rcall delay200ms
clr temp
out PORTB, temp
rcall delay200ms
ser temp			
out PORTB, temp

awaitingActivationSW0:

	sbis PIND, 0		; Check if SW0 was pressed
    rjmp activatedSW0

rjmp awaitingActivationSW0

activatedSW0:
	ser input			; Reset input
	clr myFlags			; Clearing all flags
	clr LED0State

    ser temp			; Turn off all LEDs
    out PORTB, temp
    cbi PORTB, 0		; Turn on LED0

rjmp delay5sec

setUpPass:
    sbis PIND, 1        	; If SW1 pressed then
    andi password, 0xfe 	; store 0 in password
    rcall delay20ms
    sbic PIND, 1        	; else 
    ori password, 0x01  	; store 1

    sbis PIND, 2
    andi password, 0xfd
    rcall delay20ms
    sbic PIND, 2
    ori password, 0x02

    sbis PIND, 3
    andi password, 0xfb
    rcall delay20ms
    sbic PIND, 3
    ori password, 0x04

    sbis PIND, 4
    andi password, 0xf7
    rcall delay20ms
    sbic PIND, 4
    ori password, 0x08

    sbic PIND, 7
    rjmp setUpPass

ret

;------------*****------------
; This is the main loop, if there is no input for 5 seconds, password is checked
; Delay of 5sec
; Assuming 4MHz
; Using r22, r23, r24, r25

delay5sec:
	ldi r22, 5			; Delays for 4.5 seconds
	
	outerx3_loop5:		; Everything below this point should take 1 second to execute
	    ldi r23, 5		; So when it is done we call blinkLED0Cause1Failure 

	    outer_outer_loop5:
	        ldi r24, 200

	        outer_loop5:
	            ldi r25, 255

	            inner_loop5:
	                sbis PIND, 1		; Check if any button was pressed
	                rcall SW1Pressed
	                sbis PIND, 2
	                rcall SW2Pressed
	                sbis PIND, 3
	                rcall SW3Pressed
	                sbis PIND, 4
	                rcall SW4Pressed
	                sbis PIND, 5
	                rcall SW7to5Pressed
	                sbis PIND, 6
	                rcall SW7to5Pressed
	                sbis PIND, 7
	                rcall SW7to5Pressed

	            dec r25
	            brne inner_loop5
	        dec r24
	        brne outer_loop5
	    dec r23
	    brne outer_outer_loop5

	    sbrc myFlags, 6					; If there has been 1 failed attempt
	    rcall blinkLED0Cause1Failure	; Blink LED0
	dec r22
	brne outerx3_loop5

rjmp checkPassGiven

SW1Pressed:
    sbrs input, 0		; Return if SW1 has been already pressed before
    ret 

    andi input, 0b11111110	; Clear bit 0 of input

rjmp delay5sec

SW2Pressed:
    sbrs input, 1		; Return if SW2 has been already pressed before
    ret

    sbrc input, 0		; If SW1 was not pressed before, enter Breach Mode
    rjmp checkBreach2

	SW1NotInPass:

    andi input, 0b11111101	; Clear bit 1 of input

rjmp delay5sec

SW3Pressed:
	sbrs input, 2		; Return if SW3 has been already pressed before
    ret 

    andi input, 0b11111011

rjmp delay5sec

SW4Pressed:
	sbrs input, 3		; Return if SW4 has been already pressed before
    ret

    sbrc input, 2		; If SW3 was not pressed before, enter Breach Mode
    rjmp checkBreach4

	SW3NotInPass:

    andi input, 0b11110111	; 0b1111 0111

rjmp delay5sec
    
SW7to5Pressed:
    sbrc input, 2		; If SW3 was not pressed before, enter Breach Mode
    rjmp breachMode
ret

checkPassGiven:
	cpse password, input	; If password is correct
	rjmp wrongPassword

	cbi PORTB, 0			; We do this in case LED0 was off due to blinking
	cbi PORTB, 1			; Turn on LED1

rjmp awaitingActivationSW0

wrongPassword:
	sbrc myFlags, 6				; If 6th bit of myFlags is set
	rjmp secondFailedAttempt	; then this is the 2nd failed attempt

	; 1st Failed Attempt
	ori myFlags, 0x40

	ser input

rjmp delay5sec

secondFailedAttempt:        ; Turn on/off all LEDs every 1 sec
	ser temp
	out PORTB, temp

	rcall delay1sec
	sbis PIND, 0           ; If SW0 is pressed, reset the system
	rjmp activatedSW0

	clr temp	
	out PORTB, temp

	rcall delay1sec
	sbis PIND, 0           ; If SW0 is pressed, reset the system
	rjmp activatedSW0

rjmp secondFailedAttempt

blinkLED0Cause1Failure:
	com LED0State		; Invert LED0 state every 1 second
	ori LED0State, 0xfe

	out PORTB, LED0State
ret

checkBreach2:
	sbrs password, 0   ; Check if SW1 is part of password
	rjmp breachMode    ; If it is then go to breachMode

rjmp SW1NotInPass

checkBreach4:
	sbrs password, 2   ; Check if SW3 is part of password
	rjmp breachMode    ; If it is then go to breachMode

rjmp SW3NotInPass

breachMode:
	rcall delay20ms    ; Debounce needed

	cbi PORTB, 2       ; LED2 must always be on during breach mode

	sbis PIND, 2
	cbi PORTB, 3
	
	sbis PIND, 4
	cbi PORTB, 4

	sbis PIND, 5
	cbi PORTB, 5

	sbis PIND, 6
	cbi PORTB, 6

	sbis PIND, 7
	cbi PORTB, 7
	
rjmp awaitingActivationSW0

;------------*****------------
; Delay of 20ms
; Assuming 4MHz

delay20ms:  
    push r25
    push r26    

    ldi r26, 31

    outer_loop20:

        ldi r25, 255

        inner_loop20:

            nop 
            nop
            nop
            nop
            nop
            nop
            nop
        dec r25
        brne inner_loop20
    dec r26
    brne outer_loop20

    pop r26
    pop r25

    ret

;------------*****------------
; Delay of 200ms
; By calling delay20ms 10 times

delay200ms:
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms
	rcall delay20ms

ret

;------------*****------------
; Delay of 1sec
; Assuming 4MHz

delay1sec:
	push r27
	push r26
	push r25

	ldi r27,10		; Should be 6 for 1 second delay		

	outer_outer_loop1:
		ldi r26,255

		outer_loop1:
			ldi r25,255

			inner_loop1:
				nop
				nop
				nop
				nop
				nop
			dec r25
			brne inner_loop1
		dec r26
		brne outer_loop1
	dec r27
	brne outer_outer_loop1

	pop r25
	pop r26
	pop r27
ret
